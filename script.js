const column1 = document.getElementById('1');
const column2 = document.getElementById('2');
const column3 = document.getElementById('3');
const column4 = document.getElementById('4');

const input = document.forms["myForm"]["count"];

var index = 1;


const bindData = (data) => {

    console.log(data)
    let html =  `
        <div class="gallery">
                <img src="${data.url}">
        </div>`;

    console.log(html)

    if (index == 4) {
        column4.innerHTML += html;
        index = 1;
    } else if (index == 3) {
        column3.innerHTML += html;
        index++;
    } else if (index == 2) {
        column2.innerHTML += html;
        index++;
    } else {
        column1.innerHTML +=html;
        index++;
    }

}

function getImage()  {
    fetch("https://random.dog/woof.json")
        .then(response => response.json())
        .then(json => {
            bindData(json)
        })
}

function getImages() {
    for(i = 0; i < input.value; i++) {
        getImage();
    }
}
